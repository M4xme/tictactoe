#!/usr/bin/env python3
# -*- coding:Utf-8 -*- 

from tkinter import *
from tkinter import ttk


w = Tk()                                #Instanciate TK window
w.title("Tic Tac Toe - Let's play!")    #Setting title
w.configure(background='#CFF3FD')       #Setting background

game_over = False           #Flag to check whether game is over
game_status = StringVar()   #To manage the label displaying the status
locations = []              #List to store locations of buttons
buttons = {}                #Dict to store buttons and their locations

#Create the label to display status
label = Label(textvariable=game_status,background="beige")
label.grid(row=0,column=0,padx=15,pady=15)
#Create a frame for buttons
btnbox = Frame(w)   #Création d'une frame pour les boutons
btnbox.grid(row=1, column=0,padx=15,pady=15)
game_layout = [["", "", ""], \
               ["", "", ""], \
               ["", "", ""]]

def onlClick(event):    # Left click event
    btn = event.widget  # event.widget is the widget that called the event
    btn.config(text = "X", background="beige", foreground="blue")
    game_layout[btn.row-1][btn.column] = "X"
    check_if_game_over()
def onrClick(event):    # Right click event
    btn = event.widget  # event.widget is the widget that called the event
    btn.config(text = "O", background="beige", foreground="red")
    game_layout[btn.row-1][btn.column] = "O"
    check_if_game_over()

#Create the grid of buttons
for i in range(1,4):
    for j in range(3):
        b = Button(btnbox,\
                   width=3,height=1,\
                   font=('helvetica', 20, 'bold'),\
                   background="beige")
        b.grid(row = i, column = j)     #Bound the widget to the frame
        b.bind("<Button-1>", onlClick)  #Bind left click
        b.bind("<Button-3>", onrClick)  #Bind right click
        
        locations.append(str(i)+"_"+str(j)) #Compute the key and store it
        buttons[locations[-1]] = b          #Memorize the button
        buttons[locations[-1]].row = i      #Memorize the row
        buttons[locations[-1]].column = j   #Memorize the column

#Check if winning line
def check_if_winning_line(line):
    if (line == "OOO" or line == "XXX"):
        game_over = True
        game_status.set("Game over!")

#Function to check whether a player has won
#   There are 8 lines
#     * 3 horizontals (for loop on rows)
#     * 3 verticals (for loop on columns)
#     * 2 diagonals (hard coded)
#
line = ""
def check_if_game_over():
    for i in range(3):
        #check if winning horizontal
        line = game_layout[i][0] + game_layout[i][1] + game_layout[i][2]
        check_if_winning_line(line)
        #check if winning vertical
        line = game_layout[0][i] + game_layout[1][i] + game_layout[2][i]
        check_if_winning_line(line)
    #check if winning diagonal
    line = game_layout[0][0] + game_layout[1][1] + game_layout[2][2]
    check_if_winning_line(line)
    line = game_layout[0][2] + game_layout[1][1] + game_layout[2][0]
    check_if_winning_line(line)

check_if_game_over()  #Check whether a player has won
    
#Initiating the window
w.mainloop()

sys.exit(0)